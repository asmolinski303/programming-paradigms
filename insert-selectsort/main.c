/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <stdio.h>

void select_sort(int *, int);
void insert_sort(int *, int);
void print_array(int *, int);
void swap(int *, int *);
int min_index(int *, int, int);

int main()
{
    int arr[] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
    int arr2[] = {2, 4, 1, 12, 3, 6, 8, 7, 10, 102};
    int l = 10;
    select_sort(arr, l);
    //printf("min_index: %d\n", min_index(arr2, 0, l));
    
    print_array(arr, l);
    //insert_sort(arr, l);
    //print_array(arr, l);*/

    return 0;
}

void select_sort(int *arr, int len)
{
    int start = 0;
    int index;
    int loopcounter = 0;
    for(int i = 0; i < len; i++)
    {
        index = min_index(arr, start, len);
        swap((arr + start), (arr + index));
        start++; loopcounter++;
    }
    printf("loopcounter from select_sort: %d\n", loopcounter);
}

int min_index(int *arr, int start, int end)
{
    int result = start;
    int loopcounter = 0
    for(int i = start + 1; i < end; i++)
    {
        if(arr[result] > arr[i])
        {
            result = i;
        }
        loopcouter++;
    }
    printf("loop counter from min_index %d\n: "loopcouter);
    return result;
}

void insert_sort(int *arr, int len)
{
    int loopcouter = 0;
    int next = 1;
    for(next; next < len; next++)
    {
        int curr = next;
        int tmp = arr[next];
        //printf("curr: %d, tmp: %d\n",curr, tmp);
        
        while(curr > 0 && (tmp < arr[curr - 1]))
        {
            arr[curr] = arr[curr - 1];
            curr--;
            loopcouter++;
        }
        arr[curr] = tmp;
        //print_array(arr, len);
    }
    printf("loops performed: %d\n", loopcouter);
}

void print_array(int *array, int length)
{
    for(int i = 0; i < length; i++)
    {
        printf("%d\t", *(array++) );
    }
    printf("\n");
}

void swap(int *a, int *b)
{
    int tmp = *a;
    *a = *b;
    *b = tmp;
}



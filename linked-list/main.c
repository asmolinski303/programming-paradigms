/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, PHP, Ruby, 
C#, VB, Perl, Swift, Prolog, Javascript, Pascal, HTML, CSS, JS
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <stdio.h>

typedef struct 
{
    int element;
    struct Node *next;
} Node;

typedef struct List
{
    Node root;
} List;

int is_last(Node *n);
void init(List *l, Node *n);
void print_list(List *l);
void insert_after(Node n, Node to_add);
void insert_before(Node n, Node to_add);
void append(List l, Node n);

int main()
{
    List l;
    Node root;
    root.element = 10;
    init(&l, &root);
    
    printf("List is initialized by: %d", l.root);
    return 0;
}

int is_last(Node *n)
{
    if(n->next == NULL) return 1;
    
    return 0;
}
void init(List *l, Node *n)
{
    l->root = *n;
    n->next = NULL;
}

void print_list(List *l){
    int guard =0;
    Node *act = l->root;
    Node *next = act->next;
    while(is_last(next) != 1 || guard < 10)
    {
        printf("Element: %d\t", n->element);
        next = next->next;
        guard++;
    }
}

void insert_after(Node n, Node to_add){}
void insert_before(Node n, Node to_add){}
void append(List l, Node n){}
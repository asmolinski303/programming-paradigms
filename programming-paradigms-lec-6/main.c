/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, PHP, Ruby, 
C#, VB, Perl, Swift, Prolog, Javascript, Pascal, HTML, CSS, JS
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>


typedef struct
{
    void *elems;
    int elem_size;
    int logical_len;
    int alloc_len;
    
} stack;

void stack_new(stack *s, int elem_size);
void stack_dispose(stack *s);
void stack_push(stack *s, void *elem_addr);
void stack_pop(stack *s, void *elem_addr);

int main()
{
    int top;
    stack s;
    stack_new(&s, sizeof(int));
    
    for (int i = 0; i < 100; i++)
    {
        int *pi = i;
        stack_push(&s, pi);
    }
    
    for (int i = 0; i < 5; i++)
    {
        stack_pop(&s, &top);
        printf("elem: %d\t", top);
    }
    
    stack_dispose(&s);

    return 0;
}

static void stack_resize(stack *s) // słowo static oznacza, ze nie możemy tej funkcji wywołac w innych plikach. Taki private
{
    s->alloc_len *= 2;
    s->elems = realloc(s->elems, s->alloc_len * s->elem_size);
}

void stack_new(stack *s, int elem_size)
{
    assert(elem_size > 0); 
    s->elem_size = elem_size;
    s->logical_len = 0;
    s->alloc_len = 4;
    s->elems = malloc(s->alloc_len*elem_size);
    assert(s->elems != NULL);
}

void stack_dispose(stack *s)
{
    free(s->elems);
}

void stack_push(stack *s, void *elem_addr)
{
    if(s->logical_len == s->alloc_len)
    {
        stack_resize(s);
    }
    void *target = (char*)s->elems + s->logical_len * s->elem_size; // Rzutujemy na wsakźnik, aby móc otrzymać miejsce w pamięci. Początek elemes + 
    memcpy(target, elem_addr, s->elem_size);// wskaźnik na miejsce, które chcemy wypełnić, wskaźnik na wartość wzorca do wprowadzenia, ilość przekopiowywanych bajtów
    s->logical_len++;
}

void stack_pop(stack *s, void *elem_addr) // adres elementu, w który, chcę umieścić wartość zdjętą ze stosu
{
   void *source = (char*)s->elems + ( s->logical_len - 1 ) * s->elem_size;
   memcpy(elem_addr, source, s->elem_size);
   s->logical_len--;
}

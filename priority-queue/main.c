/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, PHP, Ruby, 
C#, VB, Perl, Swift, Prolog, Javascript, Pascal, HTML, CSS, JS
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <stdio.h>

typedef struct p_element{
    int value;
    int priority;
} p_element;

typedef struct p_queue{
    p_element *elems;
    int cursor;
} p_queue;

p_element* findMin(p_queue *pq);
void insert(p_queue *pq, p_element p);
void new(p_queue *pq);
void deallocate(p_queue *pq);
void print_elems(p_queue pq);

p_element getMin(p_queue *pq);

void set_priority(p_element *p, int priority);
void set_value(p_element *p, int value);

int main()
{
    p_element p;
    set_priority(&p, 3);
    set_value(&p, 5);
    
    p_element p1;
    p_element p2;
    p_element p3;
    
    set_priority(&p1, 6);
    set_value(&p1, 100);
    
    set_priority(&p2, 3);
    set_value(&p2, 7);
    
    set_priority(&p3, 3);
    set_value(&p3, 6);
    
    p_queue pq;
    p_element *p_min;
    
    printf("elem value: %d, elem prior: %d\n", p.value, p.priority);
    printf("sizeof p_elem: %d\n", sizeof(p_element));
    
    new(&pq);
    
    insert(&pq, p);
    insert(&pq, p1);
    insert(&pq, p2);
    insert(&pq, p3);

    print_elems(pq);
    p_min = findMin(&pq);
    printf("value of most important elem: %d\n", p_min->value);
    
    deallocate(&pq);
    return 0;
}

p_element * findMin(p_queue *pq)
{
    p_element *res = &(pq->elems[0]);
    for(int i = 1; i < pq->cursor; i++)
    {
        if(res->priority < pq->elems[i].priority)
        {
            res = &(pq->elems[i]);
        }
    }
    
    return res;
}

void set_priority(p_element *p, int priority)
{
    p->priority = priority;
}

void set_value(p_element *p, int value)
{
    p->value = value;
}

void new(p_queue *pq)
{
    pq->cursor = 0;
    pq->elems = malloc(10 * sizeof(p_element));
}

void deallocate(p_queue *pq)
{
    free(pq->elems);
}

void insert(p_queue *pq, p_element p)
{
    pq->elems[pq->cursor] = p;
    pq->cursor++;
}

void print_elems(p_queue pq)
{
    for(int i = 0; i < pq.cursor; i++)
    {
        printf("elem %d has value: %d\n", i, pq.elems[i].value);
    }
}
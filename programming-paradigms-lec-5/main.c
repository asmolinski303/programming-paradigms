/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef struct 
{
    int *elem;
    int logical_len;
    int alloc_length;
} stack;

void stack_new(stack *s);
void stack_dispose(stack *s);
void stack_push(stack *s, int value);
int stack_pop(stack *s);


void *lsearch(void *key, void *base, int n, int elem_size, int (*cmpfn)(void*, void*));
int intcmp(void*, void*);
int StrCmp(void*, void*);

int main()
{
    stack s;
    stack_new(&s);
    for(int i = 0; i < 5; i++)
    {
        stack_push(&s, i);
    }
    
    for(int i = 0; i < 5; i++)
    {
        printf("stack element: %d\n", stack_pop(&s));
    }
    
    /*char* strings[] = {"ab","cd","efg","lhc","thc","bit","kitowiec"}; int size_od_strings = 7;
    char* to_find = "thc";
    
    char** found = lsearch(&to_find, strings, size_od_strings, sizeof(char*), StrCmp);
    
    printf("%s\n", *found); 
     
    int number = 400;
    int size = 8;
    int array[] = {1,2,3,400,5,6,7,8};
    int *test = lsearch(&number, array, size, sizeof(int), intcmp);
    if(test == NULL)
    {
        printf("NULL");
    } else  
    {
        printf("Jest na miejscu: %d\n", *test);
    }

    return 0;*/
}

void stack_new(stack *s)
{
    s->logical_len = 0;
    s->alloc_length = 4;
    s->elem = malloc(4 * sizeof(int));
    
    assert(s->elem != NULL);//to assure that elem has some place
}

void stack_dispose(stack *s)
{
    free(s->elem);
}

void stack_push(stack * s, int value)
{
    if(s->logical_len == s->alloc_length)
    {
        s->alloc_length *= 2;
        s->elem = realloc(s->elem, s->alloc_length*sizeof(int));
        assert(s->elem != NULL);
    }
    
    s->elem[s->logical_len] = value;
    s->logical_len++;
}

int stack_pop(stack *s)
{
    assert(s->logical_len > 0);
    s->logical_len--;
    return s->elem[s->logical_len];
}

void *lsearch(void *key, void *base, int n, int elem_size, int (*cmpfn)(void*, void*))
{
    for(int i = 0; i < n; i ++)
    {
        void *elem_addr = (char*)base + i * elem_size;
        if(cmpfn(key, elem_addr) == 0) return elem_addr;
    }
    
    return NULL;
}


int intcmp(void *elem1, void *elem2){
    
    int *ptr1 = elem1; 
    int *ptr2 = elem2; 
    return *ptr1 - *ptr2;
}

int StrCmp(void *elem1, void* elem2)
{
    char *str1 = *(char**) elem1;
    char *str2 = *(char**) elem2;
    return strcmp(str1, str2);
}


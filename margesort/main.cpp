/******************************************************************************

MergeSort - DZIAŁA W CHUJ za pierwszym 

*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

void print_array(int * , int );
void merge(int *, int, int, int);
void mergesort(int *, int);
void sort(int *, int *, int, int, int); // recursive


int main()
{
    
    int array[] = {0,5,3,8,2,1,10,13,11,14}; int len = 10;
    
    print_array(array, len);
    mergesort(array, len);
    print_array(array, len);
    
    int *aux = (int *)malloc(sizeof(int*) * len);
    /*merge(aux, a, 4, b, 4);
    
    for(int i = 0; i < 8; i++){
        printf("elemenet at %d: %d ", i, *(aux+i));
        printf("\n");
    }*/
    
    free(aux);
    
    return 0;
}

void sort(int *array, int lo, int hi)
{
    int mid = lo + (hi - lo) / 2;
    
    if(lo >= hi) return;
    sort(array, lo, mid);
    sort(array, mid + 1, hi);
    merge(array, lo, mid, hi);
}

void merge(int *array, int lo, int mid, int hi)
{
   
   //arrays should be sorted earlier
   
    int len = hi - lo + 1;
    int *aux = (int *)malloc(sizeof(int) * len);
    int i = lo; //a iter
    int j = mid + 1; //b iter
    int k = 0; //aux iter
    for(k; k < len ;k++)
    {
        if(i > mid)
        {
            aux[k] = array[j];
            j++;
        }
        else if(j > hi)
        {
            aux[k] = array[i];
            i++;
        }
        else if(array[i] <= array[j]) 
        {
            aux[k] = array[i];
            i++;
        }
        else if(array[i] > array[j])
        {
            aux[k] = array[j];
            j++;
        }
        //print_array(aux, k);
    }
    //rewrite array
    k = 0;
    for(int i = lo; i <= hi; i++)
    {
        array[i] = aux[k++];
    }
    //print_array(aux, k);
    free(aux);
    //return aux;
}

void print_array(int * array, int length)
{
    for(int i = 0; i < length; i++)
    {
        printf("%d\t", *(array++) );
    }
    printf("\n");
}

void mergesort(int *array, int len)
{
   sort(array, 0, len - 1);
}



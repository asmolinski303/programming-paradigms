/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, PHP, Ruby, 
C#, VB, Perl, Swift, Prolog, Javascript, Pascal, HTML, CSS, JS
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <stdio.h>
#include <math.h>

struct fraction {
    int num;
    int denum;
};
typedef struct fraction fraction;

struct student {
    char *name;
    char suid[8];
    int num_units;
};

typedef struct student student;

int main()
{
    
    student pupils[4];
    pupils[0].num_units = 2;
    pupils[2].name = strdup("Adam");
    pupils[3].name = pupils[0].suid + 6;
    strcpy(pupils[1].suid, "40415xx");
    strcpy(pupils[3].name, "123456x");
    printf("%d\n", pupils[0].num_units);
    printf("%s\n", pupils[1].suid);
    pupils[7].name = "adam";
    printf("%s\n", pupils[7].name);
    /*fraction pi;
    pi.num = 12;
    pi.denum = 22;
    
    ((fraction *)&(pi.denum))->num = 7; 
    ((fraction *)&(pi.num))->denum = 7; 
    
    printf("num: %d denum: %d\n", pi.num, pi.denum);*/ //part one
    
    /*int arr[5];
    arr[3] = 64;
    ((short*)arr)[7] = 2;
    printf("%d\n", arr[3]);
    printf("%f\n", (pow(2, 17) + 64));*/

    return 0;
}




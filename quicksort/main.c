/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, PHP, Ruby, 
C#, VB, Perl, Swift, Prolog, Javascript, Pascal, HTML, CSS, JS
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <stdio.h>

int partition(int *, int, int);
void swap(int *, int*);
void quicksort(int *, int, int);
void print_array(int *, int);

int main()
{
    
    int array[] = {1,7,5,2,4,12,4234,234,12,4325,56,456}; //12
    print_array(array, 12);
    quicksort(array, 0, 11);
    print_array(array, 12);

    return 0;
}

void print_array(int * array, int length)
{
    for(int i = 0; i < length; i++)
    {
        printf("%d ", *(array++) );
    }
    printf("\n");
}

void swap(int *a, int *b)
{
    int tmp = *a;
    *a = *b;
    *b = tmp;
}// works

void quicksort(int *array, int left, int right)
{
    
    if(left >= right) return;
    int k = partition(array, left, right);
    quicksort(array, left, k - 1);
    quicksort(array, k + 1, right);
}

int partition(int * array, int start, int end)
{
   int k = start;//pick first element;
   //printf("start from: %d\n", array[k]); 
   for(int i = start; i <= end; i++)
   {
       if(array[i] < array[k] && k < i)
       {
           //printf("zamieniam\n");
           swap((array + i), (array + k));
           //print_array(array, 9);
           int tmp = k;
           k = i;
           i = tmp;
       }
       else if(array[i] > array[k] && k > i )
       {
           //printf("zamieniam\n");
           swap((array + k), (array + i));
           //print_array(array, 9);
           k = i;
       }
       //printf("i: %d\tk: %d\n", i, k);
   }
   //printf("k:%d\tstart: %d\tend: %d\n", k, start, end);
   return k;
}//probably problably works